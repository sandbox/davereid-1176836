<?php

define('DEVELOPER_USERNAME', 'admin');

/**
 * Implement hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
 */
function developer_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure_form') {
    $server = $_SERVER['SERVER_NAME'];
    $mail = DEVELOPER_USERNAME . '@' . $server;

    $form['site_information']['site_name']['#default_value'] = $server;
    $form['site_information']['site_mail']['#default_value'] = $mail;
    $form['admin_account']['account']['name']['#default_value'] = DEVELOPER_USERNAME;
    $form['admin_account']['account']['mail']['#default_value'] = $mail;
    $form['admin_account']['account']['pass']['#value']['pass1'] = DEVELOPER_USERNAME;
    $form['admin_account']['account']['pass']['#value']['pass2'] = DEVELOPER_USERNAME;
    $form['admin_account']['account']['pass']['#description'] = 'The default password is <em>' . DEVELOPER_USERNAME . '</em>.';
    $form['server_settings']['site_default_country']['#default_value'] = 'US';
    $form['server_settings']['date_default_timezone']['#value'] = 'America/Chicago';
    $form['server_settings']['date_default_timezone']['#type'] = 'value';
    $form['update_notifications']['update_status_module']['#default_value'] = array(0, 0);

    $modules_available = array();
    $modules = system_rebuild_module_data();
    foreach ($modules as $index => $module) {
      if (!$module->status && empty($module->info['hidden'])) {
        $modules_available[$module->name] = $module->info['name'];
      }
    }
    $form['modules_install'] = array(
      '#type' => 'fieldset',
      '#title' => t('Modules to install'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      '#access' => !empty($modules_available),
    );
    $form['modules_install']['modules_install'] = array(
      '#type' => 'checkboxes',
      '#options' => $modules_available,
      '#default_value' => array(),
    );
    $form['#submit'][] = 'developer_install_modules';
  }
}

function developer_install_modules($form, $form_state) {
  $modules = array_filter($form_state['values']['modules_install']);
  module_enable($modules, TRUE);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function developer_form_xmlsitemap_settings_form_alter(&$form, $form_state) {
  $form['advanced']['xmlsitemap_chunk_size']['#options'][5] = 5;
}
